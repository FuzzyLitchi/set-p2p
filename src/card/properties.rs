use ggez::graphics;

#[derive(Clone, Debug, PartialEq)]
pub enum Symbol {
    Oval,
    Squiggle,
    Diamond,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Number {
    One,
    Two,
    Three,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Shading {
    Solid,
    Striped,
    Outlined,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Color {
    Red,
    Purple,
    Green,
}

impl Color {
    pub fn value(&self) -> graphics::Color {
        use self::Color::*;
        match self {
            Red    => RED,
            Purple => PURPLE,
            Green  => GREEN,
        }
    }
}

const RED: graphics::Color = graphics::Color {
    r: 0.906,
    g: 0.282,
    b: 0.298,
    a: 1.000,
};
const PURPLE: graphics::Color = graphics::Color {
    r: 0.341,
    g: 0.255,
    b: 0.580,
    a: 1.000,
};
const GREEN: graphics::Color = graphics::Color {
    r: 0.227,
    g: 0.686,
    b: 0.337,
    a: 1.000,
};