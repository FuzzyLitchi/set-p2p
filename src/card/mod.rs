use crate::sprites::Sprite;

use specs::Entity;

mod properties;
use properties::*;

#[derive(PartialEq, Clone, Debug)]
pub struct Card {
    pub color: Color,
    pub symbol: Symbol,
    pub number: Number,
    pub shading: Shading,
}

impl Card {
    pub fn from_u8(n: u8) -> Card {
        if n >= 81 {
            panic!("Invalid u8 for card. Has to be in 0..81");
        }

        let color = n % 3;
        let symbol = (n / 3) % 3;
        let number = (n / 9) % 3;
        let shading = (n / 27) % 3;

        let color = match color {
            0 => Color::Red,
            1 => Color::Purple,
            2 => Color::Green,
            _ => unreachable!(),
        };
        let symbol = match symbol {
            0 => Symbol::Oval,
            1 => Symbol::Squiggle,
            2 => Symbol::Diamond,
            _ => unreachable!(),
        };
        let number = match number {
            0 => Number::One,
            1 => Number::Two,
            2 => Number::Three,
            _ => unreachable!(),
        };
        let shading = match shading {
            0 => Shading::Solid,
            1 => Shading::Striped,
            2 => Shading::Outlined,
            _ => unreachable!(),
        };

        Card {
            color,
            symbol,
            number,
            shading,
        }
    }

    pub fn sprite(&self) -> Sprite {
        use Shading::*;
        use Symbol::*;
        use Number::*;
        use Sprite::*;
        
        match (&self.shading, &self.symbol, &self.number) {
            (Solid, Oval, One) => SolidOval1,
            (Solid, Oval, Two) => SolidOval2,
            (Solid, Oval, Three) => SolidOval3,
            (Striped, Oval, One) => StripedOval1,
            (Striped, Oval, Two) => StripedOval2,
            (Striped, Oval, Three) => StripedOval3,
            (Outlined, Oval, One) => OutlinedOval1,
            (Outlined, Oval, Two) => OutlinedOval2,
            (Outlined, Oval, Three) => OutlinedOval3,

            (Solid, Squiggle, One) => SolidSquiggle1,
            (Solid, Squiggle, Two) => SolidSquiggle2,
            (Solid, Squiggle, Three) => SolidSquiggle3,
            (Striped, Squiggle, One) => StripedSquiggle1,
            (Striped, Squiggle, Two) => StripedSquiggle2,
            (Striped, Squiggle, Three) => StripedSquiggle3,
            (Outlined, Squiggle, One) => OutlinedSquiggle1,
            (Outlined, Squiggle, Two) => OutlinedSquiggle2,
            (Outlined, Squiggle, Three) => OutlinedSquiggle3,

            (Solid, Diamond, One) => SolidDiamond1,
            (Solid, Diamond, Two) => SolidDiamond2,
            (Solid, Diamond, Three) => SolidDiamond3,
            (Striped, Diamond, One) => StripedDiamond1,
            (Striped, Diamond, Two) => StripedDiamond2,
            (Striped, Diamond, Three) => StripedDiamond3,
            (Outlined, Diamond, One) => OutlinedDiamond1,
            (Outlined, Diamond, Two) => OutlinedDiamond2,
            (Outlined, Diamond, Three) => OutlinedDiamond3,
        }
    }

    pub fn is_set(a: &Card, b: &Card, c: &Card) -> bool {
        is_partial_set(&a.color, &b.color, &c.color)
        && is_partial_set(&a.symbol, &b.symbol, &c.symbol)
        && is_partial_set(&a.number, &b.number, &c.number)
        && is_partial_set(&a.shading, &b.shading, &c.shading)
    }
}

fn is_partial_set<T: PartialEq>(a: &T, b: &T, c: &T) -> bool {
    if a == b {
        return a == c;
    } else {
        return a != c && b != c;
    }
}

pub enum CardEvent {
    Set((Entity, usize), (Entity, usize), (Entity, usize)),
}