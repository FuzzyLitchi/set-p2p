use euclid::default::*;

pub type Point = Point2D<f32>;
pub type Vector = Vector2D<f32>;
pub type Size = Size2D<f32>;

const height: usize = 3;

pub struct Vec2D<T> {
    pub vec: Vec<T>,
    pub width: usize,
}

impl<T> Vec2D<T> {
    pub fn new(vec: Vec<T>) -> Vec2D<T> {
        assert!(vec.len() % height == 0);

        Vec2D {
            width: vec.len() / height,
            vec,
        }
    }
    
    pub fn remove(&mut self, a: usize, b: usize, c: usize) {
        let mut count: [usize; 3] = [0; 3];
        let mut dead_list = [a, b, c];

        for i in 0..dead_list.len() {
            assert!(dead_list[i] < self.width*height, "Index out of bounds");

            for j in i+1..dead_list.len() {
                if dead_list[i] == dead_list[j] {
                    panic!("Vec2D.remove requires 3 unique indexes");
                }
            }
        }

        for i in dead_list.iter() {
            let (_, y) = self.index_to_coords(*i);
            count[y] += 1;
        }

        for i in 0..count.len() {
            // If a row has more than 1 dead value
            while count[i] > 1 {
                // Find last dead on this row
                let dead = self.get_row(i)
                    .filter(|i| dead_list.contains(i))
                    .last()
                    .unwrap();

                // Find row with 0 dead values
                let y = count
                    .iter()
                    .position(|c| *c == 0)
                    .unwrap();

                // Find index on that row that isn't dead.
                let alive = self.get_row(y)
                    .filter(|i| !dead_list.contains(i))
                    .last()
                    .unwrap();

                // Swap alive and dead
                self.vec.swap(dead, alive);
                let dead_i = dead_list
                    .iter()
                    .position(|d| *d == dead)
                    .unwrap();
                dead_list[dead_i] = alive;

                // Update dead count
                count[i] -= 1;
                count[y] += 1;
            }
        }

        // Decrease width
        self.width -= 1;

        // Move all dead values to the right
        for i in 0..dead_list.len() {
            let dead = dead_list[i];

            let (x, y) = self.index_to_coords(dead);

            if x < self.width {
                let mut previous = dead;
                for current in Row::new(x+1, y, self.width+1) {
                    self.vec.swap(previous, current);
                    previous = current;
                }

                dead_list[i] = previous;
            }
        }

        // Retain alive values
        let mut i = 0;
        self.vec.retain(|_| {
            let v = !dead_list.contains(&i);
            i += 1;
            v
        });
    }

    fn index_to_coords(&self, i: usize) -> (usize, usize) {
        return (i / height, i % height)
    }

    fn get_row(&self, y: usize) -> Row {
        Row {
            x: 0,
            y,
            width: self.width,
        }
    }
}

impl<T: std::fmt::Debug> Vec2D<T> {
    pub fn print(&self) {
        for y in 0..height {
            for i in self.get_row(y) {
                print!("{:?} ", self.vec[i]);
            }
            print!("\n");
        }
        print!("\n");
    }
} 

struct Row {
    x: usize,
    y: usize,
    width: usize,
}

impl Row {
    fn new(x: usize, y: usize, width: usize) -> Row {
        Row {
            x,
            y,
            width
        }
    }
}

impl Iterator for Row {
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        if self.x < self.width {
            let index = self.x * height + self.y;
            self.x += 1;

            Some(index)
        } else {
            None
        }
    }
}
