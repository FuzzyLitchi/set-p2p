use std::default::Default;

pub struct Time {
    pub time: f32,
    pub frame: u64
}

impl Default for Time {
    fn default() -> Time {
        Time {
            time: 0.0,
            frame: 0
        }
    }
}

const TIME_STEP: f32 = 1.0/60.0;
impl Time {
    pub fn update(&mut self) {
        self.frame += 1;
        self.time = self.frame as f32 * TIME_STEP;
    }
}