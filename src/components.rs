use specs::*;
use specs_derive::*;

use crate::types::*;
use crate::sprites::Sprite;

// This file contains every component. Components are simply storage for some data,
// they only become useful when systems interact with them. However they are a useful
// abstraction for organizing our game, and therefor we use them.

// A position in the game world.
#[derive(Clone, Debug, Component)]
#[storage(VecStorage)]
pub struct Position(pub Point);

// Linear interpolation from one point to another
#[derive(Clone, Debug, Component)]
#[storage(VecStorage)]
pub struct Lerp {
    // to and from positions
    pub to: Point,
    pub from: Point,
    // start time
    pub start: f32,
    pub duration: f32,
}

// Area you can click and select
#[derive(Clone, Debug, Component)]
#[storage(VecStorage)]
pub struct Selectable {
    pub selected: bool,
    pub size: Size,
}

impl Selectable {
    pub fn new(size: Size) -> Selectable {
        Selectable {
            selected: false,
            size,
        }
    }
}

// Card data
#[derive(Clone, Debug, Component)]
#[storage(VecStorage)]
pub struct CardId(pub usize);

// A tag to enable redering for the entity
#[derive(Clone, Debug, Component)]
#[storage(VecStorage)]
pub enum Renderable {
    Rectangle {
        w: f32,
        h: f32,
        color: ggez::graphics::Color
    },
    Sprite(Sprite),
    Card {
        card: Sprite,
        face: Sprite,
        color: ggez::graphics::Color,
    },
}

// We add every component to our specs world
pub fn register_components(specs_world: &mut World) {
    specs_world.register::<Position>();
    specs_world.register::<Lerp>();
    specs_world.register::<Renderable>();
}
