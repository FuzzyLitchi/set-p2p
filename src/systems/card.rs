use crate::components::*;
use crate::resources::*;
use crate::{
    card::{Card, CardEvent},
    table::{self, Table},
    types::*,
    sprites::Sprite,
};

use specs::*;
use shrev::*;

#[derive(Default)]
pub struct CardSystem {
    reader: Option<ReaderId<CardEvent>>,
}

impl<'a> System<'a> for CardSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, EventChannel<CardEvent>>,
        Write<'a, Table>,
        Read<'a, Time>,
        Read<'a, LazyUpdate>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, CardId>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            entities,
            events,
            mut table,
            time,
            updater,
            positions,
            mut cards,
        ) = data;

        for event in events.read(self.reader.as_mut().unwrap()) {
            if let CardEvent::Set(a, b, c) = event {
                let mut rng = rand::thread_rng();

                // Delete old card entity
                for (entity, _) in [a, b, c].iter() {
                    entities.delete(*entity).expect("Card entity doesn't exist");
                }

                // Cards to add the the ECS
                let mut new_cards: Vec<usize> = Vec::new();
                
                // Used to check if we should reorder all cards
                let mut reorder = false;

                // We have enough cards left, deal new ones
                if table.deck.len() >= 3 && table.placed.len() == 12 {
                    // Draw new cards and update table
                    for (_, card_id) in [a, b, c].iter() {
                        // Draw new card
                        let card = table.draw_card(&mut rng);

                        // Add to new cards
                        new_cards.push(*card_id);

                        // Update table
                        table.replace_card(*card_id, card);
                    }
                } else {
                    // There is either no cards left in the deck, or we previously had too many cards
                    // and therefore we need to reduce the amount of cards in placed card.
                    let mut vec: Vec<(Entity, usize)> = Vec::new();
                    for (card_id, entity) in (&cards, &entities).join() {
                        vec.push((entity, card_id.0));
                    }
                    vec.sort_unstable_by(|(_, a), (_, b)| a.cmp(b));

                    let vec: Vec<(Entity, Card)> = vec.iter().map(|(e, card_id)| (*e, table.get(*card_id).clone()))
                        .collect();
                    
                    let mut vec2d = Vec2D::new(vec);
                    vec2d.remove(a.1, b.1, c.1);

                    for (card_id, (entity, _)) in vec2d.vec.iter().enumerate() {
                        let card: &mut CardId = cards.get_mut(*entity).unwrap();
                        card.0 = card_id;
                    }

                    table.placed = vec2d.vec.iter().map(|(_, card)| card.clone()).collect();
                    table.width = vec2d.width as u32;
                    reorder = true;
                }

                // Check if there is a set among the new cards, if there isn't add cards until there is
                // The rules of Set require there to be a set among 21 cards, so we can be sure that
                // we don't need more than 7 collumns.
                while !table.set_exists() && table.deck.len() >= 3 { // TODO change this when implementing win condition
                    // Resize table
                    table.width += 1;
                    reorder = true;

                    for _ in 0..3 {
                        let card = table.draw_card(&mut rng);

                        // push card_id
                        new_cards.push(table.placed.len());

                        table.placed.push(card);
                    }
                }

                // Move existing cards to fit resized table
                if reorder {
                    for (pos, card_id, entity) in (&positions, &cards, &entities).join() {
                        updater.insert(entity, Lerp {
                            to: table.index_to_pos(card_id.0),
                            from: pos.0.clone(),
                            start: time.time,
                            duration: 1.0
                        });
                    }
                }

                // Add cards to ECS and do animations
                for (i, card_id) in new_cards.iter().enumerate() {
                    // Add it to the ECS
                    let card = table.get(*card_id);

                    let entity = entities.create();
                    updater.insert(entity, Position(Point::new(-50.0, 0.0)));
                    updater.insert(entity, Selectable::new(
                        Size::new(table::CARD_WIDTH, table::CARD_HEIGHT)
                    ));
                    updater.insert(entity, Lerp {
                        to: table.index_to_pos(*card_id),
                        from: Point::new(-50.0, 0.0),
                        start: time.time + 0.5 * i as f32,
                        duration: 1.0
                    });
                    updater.insert(entity, Renderable::Card {
                        card: Sprite::Card,
                        face: card.sprite(),
                        color: card.color.value(),
                    });
                    updater.insert(entity, CardId(*card_id));
                }
            }
        }
    }

    fn setup(&mut self, world: &mut World) {
        Self::SystemData::setup(world);
        self.reader = Some(world.fetch_mut::<EventChannel<CardEvent>>().register_reader());
    }
}
