use crate::components::*;
use crate::input::{self, Button};
use crate::table::Table;
use crate::card::*;

use specs::*;
use shrev::EventChannel;
use euclid::Rect;

use std::collections::HashSet;

#[derive(Default)]
pub struct SelectSystem {
    selected: HashSet<Entity>,
}

impl<'a> System<'a> for SelectSystem {
    type SystemData = (
        WriteStorage<'a, Selectable>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, CardId>,
        Read<'a, Table>,
        Read<'a, input::State>,
        Entities<'a>,
        Write<'a, EventChannel<CardEvent>>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            mut selectable,
            pos,
            cards,
            table,
            input,
            entities,
            mut events,
        ) = data;

        if input.get_button_pressed(Button::Left) {
            for (mut selectable, pos, card, entity) in (&mut selectable, &pos, &cards, &entities).join() {
                let area = Rect::new(pos.0, selectable.size);

                if area.contains(input.mouse_position()) {
                    // Find entity in list
                    if self.selected.insert(entity) {
                        // Selected
                        selectable.selected = true;
                        println!("Selected: {}", card.0);
                    } else {
                        // Was already selected, deselect
                        selectable.selected = false;
                        self.selected.remove(&entity);
                    }
                }
            }

            println!("{}", self.selected.len());
        }

        // Move this somewhere else tbh
        if self.selected.len() == 3 {
            let ids: Vec<(Entity, usize)> = self.selected.iter()
                .map(|entity| (*entity, cards.get(*entity).unwrap().0))
                .collect();
            
            if table.is_set(ids[0].1, ids[1].1, ids[2].1) {

                // Send cards
                events.single_write(
                    CardEvent::Set(ids[0], ids[1], ids[2])
                );
                
                // Clear the selection
                self.selected.clear();
            }
        }
    }
}