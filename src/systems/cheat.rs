use crate::components::*;
use crate::input::{self, Button};
use crate::table::Table;
use crate::card::*;

use specs::*;
use shrev::EventChannel;
use euclid::Rect;

use std::collections::HashSet;

#[derive(Default)]
pub struct CheatSystem;

impl<'a> System<'a> for CheatSystem {
    type SystemData = (
        Read<'a, Table>,
        Read<'a, input::State>,
        Write<'a, EventChannel<CardEvent>>,
        Entities<'a>,
        ReadStorage<'a, CardId>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            table,
            input,
            mut events,
            entities,
            cards,
        ) = data;

        if input.get_button_pressed(Button::Right) {
            if let Some((a, b, c)) = table.find_set() {
                let mut set: Vec<(Entity, usize)> = Vec::new();

                for (card_id, entity) in (&cards, &entities).join() {
                    if [a, b, c].contains(&card_id.0) {
                        set.push((entity, card_id.0));
                    }
                }
                
                events.single_write(
                    CardEvent::Set(set[0], set[1], set[2])
                );
            }
        }
    }
}