use crate::components::*;
use crate::resources::*;

use specs::*;

pub struct LerpSystem;

impl<'a> System<'a> for LerpSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, Lerp>,
        Read<'a, LazyUpdate>,
        Read<'a, Time>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            entities,
            mut positions,
            lerps,
            updater,
            time,
        ) = data;

        for (entity, mut pos, lerp) in (&entities, &mut positions, &lerps).join() {
            // If we've pased the end of the animation
            if time.time > lerp.start + lerp.duration {
                pos.0 = lerp.to;
                updater.remove::<Lerp>(entity);
                continue;
            } else if time.time < lerp.start {
                continue;
            }

            // From 0.0-1.0
            let progress = (time.time - lerp.start)/lerp.duration;
            pos.0 = lerp.from + (lerp.to-lerp.from)*(2.0*progress-progress.powi(2));
        }
    }
}