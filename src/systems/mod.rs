// specs systems.
use specs;

mod lerp;
mod select;
mod card;
mod cheat;

// Create specs dispatcher with systems
pub fn register_systems() -> specs::Dispatcher<'static, 'static> {
    specs::DispatcherBuilder::new()
        .with(lerp::LerpSystem, "lerp", &[])
        .with(select::SelectSystem::default(), "select", &[])
        .with(card::CardSystem::default(), "card", &["select"])
        .with(cheat::CheatSystem, "cheat", &[])
        .build()
}