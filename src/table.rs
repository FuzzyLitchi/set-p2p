use crate::card::Card;
use rand::{rngs::ThreadRng, Rng};
use crate::types::Point;

pub struct Table {
    pub deck: Vec<Card>,
    pub placed: Vec<Card>,
    discarded: Vec<Card>,
    pub width: u32, // Amount of collumns
    pixel_width: f32,
    pixel_height: f32,
}

impl Default for Table {
    fn default() -> Table {
        unimplemented!();
    }
}

pub const CARD_WIDTH: f32 = 27.0;
pub const CARD_HEIGHT: f32 = 38.0;

impl Table {
    pub fn new() -> Table {
        let mut table = Table {
            deck: (0..81).map(Card::from_u8).collect(),
            placed: Vec::with_capacity(12),
            discarded: Vec::new(),
            width: 4,
            pixel_width: 200.0,
            pixel_height: 150.0
        };

        let mut rng = rand::thread_rng();

        for _ in 0..12 {
            let card = table.draw_card(&mut rng);
            table.placed.push(card);
        }
        
        table
    }

    pub fn draw_card(&mut self, rng: &mut ThreadRng) -> Card {
        self.deck.remove(rng.gen_range(0, self.deck.len()))
    }

    pub fn replace_card(&mut self, i: usize, card: Card) {
        let old_card = self.placed[i].clone();
        self.placed[i] = card;
        self.discarded.push(old_card);
    }

    pub fn set_exists(&self) -> bool {
        self.find_set().is_some()
    }

    pub fn find_set(&self) -> Option<(usize, usize, usize)> {
        let n = self.placed.len();

        for a in 0..n {
            for b in a+1..n {
                for c in b+1..n {
                    if self.is_set(a, b, c) {
                        return Some((a, b, c));
                    }
                }
            }
        }
        
        None
    }

    pub fn get(&self, i: usize) -> &Card {
        &self.placed[i]
    }

    // Starts from top-left corner and moves down, jumps back up to the next collumn
    // pub fn get_from_pos(&self, x: usize, y: usize) -> &Card {
    //     &self.placed[x * 3 + y]
    // }

    pub fn index_to_pos(&self, i: usize) -> Point {
        let (x, y) = (i / 3, i % 3);
        self.pos(x, y)
    }

    fn pos(&self, x: usize, y: usize) -> Point {
        let margin_x = self.pixel_width/self.width as f32 - CARD_WIDTH;
        let margin_y = self.pixel_height/3.0 - CARD_HEIGHT;

        Point::new(
            x as f32 * self.pixel_width/self.width as f32 + margin_x/2.0,
            y as f32 * self.pixel_height/3.0 + margin_y/2.0
        )
    }

    pub fn is_set(&self, a: usize, b: usize, c: usize) -> bool {
        Card::is_set(
            self.get(a),
            self.get(b),
            self.get(c),
        )
    }
}