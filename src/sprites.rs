use std::collections::HashMap;
use ggez::graphics::{Image, DrawParam, Rect};

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
pub enum Sprite {
    Card,

    SolidOval1,
    SolidOval2,
    SolidOval3,
    StripedOval1,
    StripedOval2,
    StripedOval3,
    OutlinedOval1,
    OutlinedOval2,
    OutlinedOval3,

    SolidSquiggle1,
    SolidSquiggle2,
    SolidSquiggle3,
    StripedSquiggle1,
    StripedSquiggle2,
    StripedSquiggle3,
    OutlinedSquiggle1,
    OutlinedSquiggle2,
    OutlinedSquiggle3,

    SolidDiamond1,
    SolidDiamond2,
    SolidDiamond3,
    StripedDiamond1,
    StripedDiamond2,
    StripedDiamond3,
    OutlinedDiamond1,
    OutlinedDiamond2,
    OutlinedDiamond3,
}



// We load all the sprites into ggez and return a hashmap of enum value to image reference
pub fn load_sprites(ctx: &mut ggez::Context) -> HashMap<Sprite, (Image, DrawParam)> {
    use Sprite::*;

    let mut sprites = HashMap::new();

    // 9x4 sprites of 27x38 pixels each
    let cards = SpriteSheet::new(
        Image::new(ctx, "/card.png").unwrap(),
        9, 4
    );
    cards.load_sprite(&mut sprites, Card, 0, 0);

    // Ovals
    cards.load_sprite(&mut sprites, SolidOval1, 0, 1);
    cards.load_sprite(&mut sprites, SolidOval2, 1, 1);
    cards.load_sprite(&mut sprites, SolidOval3, 2, 1);
    cards.load_sprite(&mut sprites, StripedOval1, 3, 1);
    cards.load_sprite(&mut sprites, StripedOval2, 4, 1);
    cards.load_sprite(&mut sprites, StripedOval3, 5, 1);
    cards.load_sprite(&mut sprites, OutlinedOval1, 6, 1);
    cards.load_sprite(&mut sprites, OutlinedOval2, 7, 1);
    cards.load_sprite(&mut sprites, OutlinedOval3, 8, 1);

    // Squiggle
    cards.load_sprite(&mut sprites, SolidSquiggle1, 0, 2);
    cards.load_sprite(&mut sprites, SolidSquiggle2, 1, 2);
    cards.load_sprite(&mut sprites, SolidSquiggle3, 2, 2);
    cards.load_sprite(&mut sprites, StripedSquiggle1, 3, 2);
    cards.load_sprite(&mut sprites, StripedSquiggle2, 4, 2);
    cards.load_sprite(&mut sprites, StripedSquiggle3, 5, 2);
    cards.load_sprite(&mut sprites, OutlinedSquiggle1, 6, 2);
    cards.load_sprite(&mut sprites, OutlinedSquiggle2, 7, 2);
    cards.load_sprite(&mut sprites, OutlinedSquiggle3, 8, 2);
    
    // Diamond
    cards.load_sprite(&mut sprites, SolidDiamond1, 0, 3);
    cards.load_sprite(&mut sprites, SolidDiamond2, 1, 3);
    cards.load_sprite(&mut sprites, SolidDiamond3, 2, 3);
    cards.load_sprite(&mut sprites, StripedDiamond1, 3, 3);
    cards.load_sprite(&mut sprites, StripedDiamond2, 4, 3);
    cards.load_sprite(&mut sprites, StripedDiamond3, 5, 3);
    cards.load_sprite(&mut sprites, OutlinedDiamond1, 6, 3);
    cards.load_sprite(&mut sprites, OutlinedDiamond2, 7, 3);
    cards.load_sprite(&mut sprites, OutlinedDiamond3, 8, 3);

    sprites
}

struct SpriteSheet {
    image: Image,
    width: f32,
    height: f32,
}

impl SpriteSheet {
    fn new(image: Image, width: u8, height: u8) -> SpriteSheet {
        SpriteSheet {
            image,
            width: width as f32,
            height: height as f32,
        }
    }

    fn load_sprite(&self, sprites: &mut HashMap<Sprite, (Image, DrawParam)>, sprite: Sprite, x: u8, y: u8) {
        if sprites.contains_key(&sprite) {
            panic!("Can't assign two sprites to sprite enum {:?}", sprite);
        }
        
        sprites.insert(sprite, (
            self.image.clone(),
            DrawParam::default()
                .src(Rect::new(
                    (x) as f32 / self.width,
                    (y) as f32 / self.height,
                    1.0 / self.width,
                    1.0 / self.height,
                ))
        ));
    }
}
