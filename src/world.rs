use crate::{
    input,
    systems,
    components,
    sprites::{self, Sprite},
    table::{self, Table},
    resources,
    types::*,
};

use ggez::graphics::*;
use specs::{self, world::Builder, WorldExt};

use std::collections::HashMap;

// The game world. Every entity lives in here.
pub struct World {
    // ECS
    specs_world: specs::World, // Contains components and entities
    dispatcher: specs::Dispatcher<'static, 'static>, // Contains systems

    // Meshes for rendering
        // Sprites are loaded upen world initialization and
        // aren't supposed to change after that
    sprites: HashMap<Sprite, (Image, DrawParam)>,
    square: Mesh, // Mesh for rendering rectangles

    // Scaling
    scale: f32,
}

impl World {
    pub fn new(ctx: &mut ggez::Context) -> Self {
        // Create empty specs world
        let mut specs_world = specs::WorldExt::new();
        components::register_components(&mut specs_world);

        // Add input state
        specs_world.insert(input::State::new());
        // Add time
        specs_world.insert(resources::Time::default());
        // Add table
        specs_world.insert(Table::new());

        let mut dispatcher = systems::register_systems();
        dispatcher.setup(&mut specs_world);

        // Add mesh for debug square rendering
        let square = Mesh::new_rectangle(
            ctx,
            DrawMode::fill(),
            Rect::new(0.0, 0.0, 1.0, 1.0),
            Color::new(1.0, 1.0, 1.0, 1.0)
        ).unwrap();

        // Add images for sprite rendering
        let sprites = sprites::load_sprites(ctx);

        let mut the_world = Self {
            specs_world,
            dispatcher,
            square,
            sprites,
            scale: 4.0,
        };

        for i in 0..12 {
            let card;
            let card_pos;
            {
                let table = the_world.specs_world.fetch::<Table>();
                card = table.get(i).clone();
                card_pos = table.index_to_pos(i);
            }

            // Create card
            the_world.specs_world
                .create_entity()
                .with(components::Position(Point::new(0.0, 0.0)))
                .with(components::Selectable::new(
                    Size::new(table::CARD_WIDTH, table::CARD_HEIGHT)
                ))
                .with(components::Lerp {
                    to: card_pos,
                    from: Point::new(0.0, 0.0),
                    start: 0.0,
                    duration: 2.0
                })
                .with(components::Renderable::Card {
                    card: Sprite::Card,
                    face: card.sprite(),
                    color: card.color.value(),
                })
                .with(components::CardId(i))
                .build();
        }

        the_world
    }

    pub fn update(&mut self, _ctx: &mut ggez::Context) {
        // Run systems
        self.dispatcher.dispatch(&mut self.specs_world);
        
        // Maintain (cleans up deleted entities/components and adds new ones)
        self.specs_world.maintain();

        // Update input state
        // This has to be after the dispatch. Order is important for get_button_pressed and _released.
        self.specs_world.fetch_mut::<input::State>().update();
        self.specs_world.fetch_mut::<resources::Time>().update();
    }

    pub fn draw(&mut self, ctx: &mut ggez::Context) -> ggez::GameResult<()> {
        use components::{Renderable, Position};
        use specs::{Join, ReadStorage};

        // Set scaling
        let matrix = DrawParam::default()
            .scale(Vector::new(self.scale, self.scale))
            .to_matrix();

        push_transform(ctx, Some(matrix));
        apply_transformations(ctx)?;

        // We can draw every entity that has both a position and a renderable component
        let (renderable, position): (ReadStorage<Renderable>, ReadStorage<Position>) = self.specs_world.system_data();
        
        // .join() to make sure we only get entities that have both
        for (renderable, position) in (&renderable, &position).join() {
            match renderable {
                // Draw rectangle
                Renderable::Rectangle {w, h, color} => draw(
                    ctx,
                    &self.square,
                    DrawParam::default()
                        .dest(position.0)
                        .scale(Vector::new(*w, *h))
                        .color(*color)
                )?,
                // Draw sprite
                Renderable::Sprite(sprite) => {
                    let (image, param) = self.sprites.get(sprite).unwrap();

                    draw(
                        ctx,
                        image,
                        param.dest(position.0)
                    )?;
                },
                // Draw card (kinda yikes tbh)
                Renderable::Card {card, face, color} => {
                    let (image, param) = self.sprites.get(card).unwrap();
                    draw(ctx, image, param.dest(position.0))?;

                    let (image, param) = self.sprites.get(face).unwrap();
                    draw(ctx, image, param.dest(position.0).color(*color))?;
                },
            }
        }

        Ok(())
    }

    pub fn handle_input(&mut self, ev: input::Event, started: bool) {
        self.specs_world.fetch_mut::<input::State>()
            .update_effect(ev, started);
    }

    pub fn handle_mouse_motion(&mut self, x: f32, y: f32) {
        self.specs_world.fetch_mut::<input::State>()
            .update_mouse_position(x/self.scale, y/self.scale);
    }
}
