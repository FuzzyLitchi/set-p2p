# Peer-to-peer Set card game

This is the second time I create the card game Set. This version is built using specs (an ECS library) and ggez (for rendering, windowing and assorted other features).

The goal is to eventually include online play against a variable amount of players without using a server. This requires the usage of some kind of NAT traversal of packet relay and protocols for agreeing to f.x. random numbers as a group.

[libp2p](https://docs.libp2p.io/concepts/nat/) has good documentation of popular NAT-traversal techniques.
[Wikipedia](https://en.wikipedia.org/wiki/Set_(card_game)) has a good explenation of the game rules and mechanics.

So far the player can select cards and upon a correct set we remove those cards and add new.
The game also recognizes when there are no cards and adds an additional 3.

TODO:
 - [ ] "Win" conditions
 - [ ] Menu and scene system
 - [ ] 1v1 Online play
 - [ ] Variable player count play

The graphics and ux can also be substantially improved.
